﻿using UnityEngine;
using System.Collections;
using System;

public class ProfessionJobless : CharacterProfessions
{

    protected override int SetProfessionID(int professionID)
    {
        professionID = 0;
        return professionID;
    }

    protected override GetBaseStats SetBaseStats(int STR, int VIT, int INT, int SPR, int SPD, int baseHP, int baseMP, int DP)
    {
        STR = 0;
        VIT = 0;
        INT = 0;
        SPR = 0;
        SPD = 0;
        baseHP = 0;
        baseMP = 0;
        DP = 100;
        return new GetBaseStats(STR, VIT, INT, SPR, SPD, baseHP, baseMP, DP);
    }

    protected override GetStatsPerLv SetStatsPerLV(int STRperLv, int VITperLv, int INTperLv, int SPRperLv, int SPDperLv)
    {
        STRperLv = 0;
        VITperLv = 0;
        INTperLv = 0;
        SPRperLv = 0;
        SPDperLv = 0;
        return new GetStatsPerLv(STRperLv, VITperLv, INTperLv, SPRperLv, SPDperLv);
    }
}
