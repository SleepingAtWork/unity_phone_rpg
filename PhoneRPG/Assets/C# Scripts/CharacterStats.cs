﻿using UnityEngine;
using System.Collections;

// Use this class to create and calculate Character Stat Points
// Class includes Main Stats (STR, VIT, INT, SPR, SPD, etc.) and Sub Stats (ATK, MATK, DEF, HP, etc.)
public class CharacterStats : MonoBehaviour
{
    // Main Stats here
    int STR, VIT, INT, SPR, SPD;

    // Sub Stats here
    float ATK, MATK, DEF, HP, MP, DP;

    // Hidden Stats here
    float mastery, bonusDamage, driveDamage, damageReduction;

    // Update is called once per frame
    void Update ()
    {
        
	}
}
