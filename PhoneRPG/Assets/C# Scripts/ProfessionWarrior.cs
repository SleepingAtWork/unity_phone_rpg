﻿using UnityEngine;
using System.Collections;

public class ProfessionWarrior : ProfessionJobless
{
    // Set ProfessionID here
    private int professionID = 1;

    // Set Base Stats Here
    private int STR = 9, 
        VIT = 6, 
        INT = 1, 
        SPR = 1, 
        SPD = 3, 
        baseHP = 160, 
        baseMP = 5, 
        DP = 100;

    // Set Stats Per Level Here
    private int STRperLv = 4,
        VITperLv = 3,
        INTperLv = 1,
        SPRperLv = 1,
        SPDperLv = 1;

    // All you need to do is instantiate the profession object "ProfessionNAME"
    public ProfessionWarrior()
    {
        SetProfessionID(professionID);
        SetBaseStats(STR, VIT, INT, SPR, SPD, baseHP, baseMP, DP);
        SetStatsPerLV(STRperLv, VITperLv, INTperLv, SPRperLv, SPDperLv);
    }
    protected override int SetProfessionID(int professionID)
    {
        return professionID;
    }

    protected override GetBaseStats SetBaseStats(int STR, int VIT, int INT, int SPR, int SPD, int baseHP, int baseMP, int DP)
    {
        return new GetBaseStats(STR, VIT, INT, SPR, SPD, baseHP, baseMP, DP);
    }

    protected override GetStatsPerLv SetStatsPerLV(int STRperLv, int VITperLv, int INTperLv, int SPRperLv, int SPDperLv)
    {
        return new GetStatsPerLv(STRperLv, VITperLv, INTperLv, SPRperLv, SPDperLv);
    }
}
