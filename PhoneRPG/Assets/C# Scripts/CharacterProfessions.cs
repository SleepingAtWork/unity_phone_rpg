﻿using UnityEngine;
using System.Collections;

// Use this class as an abstract for inheritance
// This class handles all character professions
// Don't actually use this class to call for profession data, use the correct profession called
// See additional txt file for list of professionIDs
public abstract class CharacterProfessions
{
    protected abstract int SetProfessionID(int professionID);
    protected abstract GetBaseStats SetBaseStats(int STR, int VIT, int INT, int SPR, int SPD, int baseHP, int baseMP, int DP);
    protected abstract GetStatsPerLv SetStatsPerLV(int STRperLv, int VITperLv, int INTperLv, int SPRperLv, int SPDperLv);

    protected const float HPperLVMod = 0.215F;
    protected const float MPperLVMod = 0.135F;

    // This two struct are used to pass stats between inherited classes
    protected struct GetBaseStats
    {
        public int STR, VIT, INT, SPR, SPD, baseHP, baseMP, DP;
        public GetBaseStats(int STRpass, int VITpass, int INTpass, int SPRpass, int SPDpass, int baseHPpass, int baseMPpass, int DPpass)
        {
            STR = STRpass;
            VIT = VITpass;
            INT = INTpass;
            SPR = SPRpass;
            SPD = SPDpass;
            baseHP = baseHPpass;
            baseMP = baseMPpass;
            DP = DPpass;
        }
            
    }
    protected struct GetStatsPerLv
    {
        public int STRperLv, VITperLv, INTperLv, SPRperLv, SPDperLv;
        public GetStatsPerLv(int STRperLvpass, int VITperLvpass, int INTperLvpass, int SPRperLvpass, int SPDperLvpass)
        {
            STRperLv = STRperLvpass;
            VITperLv = VITperLvpass;
            INTperLv = INTperLvpass;
            SPRperLv = SPRperLvpass;
            SPDperLv = SPDperLvpass;
        }
    }

}
